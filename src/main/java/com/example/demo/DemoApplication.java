package com.example.demo;

import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
@Slf4j
public class DemoApplication {

	@GetMapping("/")
	String home() {
		return "Spring is here!";
	}

	@PostConstruct
	public void init(){
		log.info("STARTRINGGASNJKADNJKABYUQWBEU");
		log.info("STARTEDNASDJKNLAWNE");
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}